# The Fun Learning Tree

Fun learning-tree game aimed for every audience to learn about out-of-school skills, knowledge & conditioning. The game is build around psychological theories of concentration, motivation and self-development.